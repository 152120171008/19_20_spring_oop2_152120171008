﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
namespace OOPLab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static string getHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
        string uname;
        string passw;
        private void button1_Click(object sender, EventArgs e)
        {
            var list = new List<string>();
            FileStream filer = new FileStream("data.csv", FileMode.Open, FileAccess.Read);
            using (StreamReader reader = new StreamReader(filer))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                }
                reader.Close();
            }
            
            char[] delimiterChars = { ' ',';' };
            string words = string.Join(";", list);
            string[] word = words.Split(delimiterChars);
            int temp = 0;
            for (int i = 0; i < word.Count(); i++)
            {
                if (word[i] == textBox1.Text)
                    temp = i;
                uname = word[temp];
                passw = word[temp + 1];
            }
            if ((textBox1.Text == uname && getHashSha256(textBox2.Text) == passw))
            {
                label3.Text = "SUCCESS";
                label3.ForeColor = Color.Green;
                timer1.Interval = 3000;
                timer1.Enabled = true;
            }
            else
            {
                label3.Text = "FAILURE";
                label3.ForeColor = Color.Red;
                textBox1.Clear();
                textBox2.Clear();
            }
            if (checkBox1.Checked == true)
            {
                Properties.Settings.Default.username = textBox1.Text;
                Properties.Settings.Default.password = textBox2.Text;
                Properties.Settings.Default.Save();
            }
            if (checkBox1.Checked == false)
            {
                Properties.Settings.Default.username = "";
                Properties.Settings.Default.password = "";
                Properties.Settings.Default.Save();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 kayıt = new Form3();
            kayıt.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Hide();
            Form2 yeni = new Form2();
            yeni.Show();
            timer1.Enabled = false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if(Properties.Settings.Default.username!=string.Empty)
            {
                textBox1.Text = Properties.Settings.Default.username;
                textBox2.Text = Properties.Settings.Default.password;
                checkBox1.Checked = true;
            }
        }
    }
}
