﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace OOPLab
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FileStream file = new FileStream("data.csv", FileMode.Append);
            StreamWriter write = new StreamWriter(file);
            write.Write(textBox1.Text+";"+ User.getHashSha256(textBox2.Text) + "\n");
            write.Close();
            file.Close();
            textBox1.Clear();
            textBox2.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 yeni = new Form1();
            yeni.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Metin Dosyası|*.tsv";
            save.OverwritePrompt = true;
            save.CreatePrompt = true;

            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter Kayit = new StreamWriter(save.FileName);
                Kayit.Write(textBox1.Text);
                Kayit.Write("\t");
                Kayit.Write(User.getHashSha256(textBox2.Text) + "\n");
                Kayit.Close();
            }
        }
    }
}
